# Porton Levadizo
El proyecto consiste en un porton levadizo, el cual se controla mediante mandos de RF a distancia.

HARDWARE:

Para el funcionamiento del porton se utilizaron dos motores que funcionan con 220VCA los cuales cumplen el papel de ascender y descender el porton.

-Una placa de relés para accionar de manera automatica a traves de programacion los motores para ascenso y descenso.

-Un modulo de RF 433MHz junto a su respectivo mando 

-Fusibles para proteger el equipo de sobrecargas de tension.

-Un Arduino Uno para leer las señales de los mandos de RF y activar los correspondientes relés

-Una fuente de 5[v] 1[A] para suministrar energia al arduino

SOFTWARE:

Para ello se utiliza un Arduino uno, en el cual se cargó un programa que nos permite captar las señales de los mandos disponibles a traves del puerto serie, para asi poder agendar las id correspondientes a cada mando y permitir que el porton se active unicamente con el mando deseado. Una vez configurado los mandos se procede a las condiciones bajo las cuales se debe activar cada rele, lo cual va a depender de los fines de carrera que envian una señal al arduino anunciandole la respectiva posicion del porton. Asi mismo en caso de detener el porton a mitad de trayecto (caso en el cual no se activa ningun relé por ejemplo) se conserva en memoria el ultimo movimiento efectuado, es decir si el porton estaba subiendo y se detiene a mitad de camino, el porton va a seguir subiendo al accionarse nuevamente.
 
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
