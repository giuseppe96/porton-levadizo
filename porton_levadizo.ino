#include <RCSwitch.h>
#define RELE1 4
#define RELE2 5
#define PULSADOR1 8
#define PULSADOR2 9
#define boton1 158733253
#define boton2 158733237
#define boton3 134833029
#define boton4 134833013
#define boton5 171533045 //ESTE MANDO  (VERSION 1.2) funca igual
#define boton6 171533029 //""   ""         ""
#define boton7 185333341 //"" ""          ""
#define boton8 185333325 // "" ""         ""

RCSwitch mySwitch = RCSwitch();

void setup() 
{
pinMode(PULSADOR1,INPUT_PULLUP);//SENSOR MAGNETICO1
pinMode(PULSADOR2,INPUT_PULLUP);//SENSOR MAGNETICO2
pinMode(13, OUTPUT);        //LED
pinMode(RELE1,OUTPUT);      //RELE1
pinMode(RELE2,OUTPUT);      //RELE2
pinMode(11,OUTPUT);         //LED DE AVISO
Serial.begin(9600);
mySwitch.enableReceive(0);  //es el pin 2 de arduino uno (lo establezco como rx)

}

void loop() 
{
int finaldecarrera1=digitalRead(PULSADOR1);
int finaldecarrera2=digitalRead(PULSADOR2);
int rele1=digitalRead(RELE1);
int rele2=digitalRead(RELE2);

  if(mySwitch.available())
  {
    long int value = mySwitch.getReceivedValue();
   
    if(value==0)
    {    
        Serial.println("error de codigo");
    }
    else{
      Serial.print("codigo recibido");
      Serial.println(value);
      Serial.println(mySwitch.getReceivedValue());
      
     if(finaldecarrera1==LOW && value==boton1 && rele2==LOW && finaldecarrera2==HIGH) //sube el porton
  {
   digitalWrite(RELE1,HIGH);
   //digitalWrite(13,HIGH);
   delay(100);
   Serial.println("subiendo");
   //mySwitch.resetAvailable();   //al no terminarse la instruccion del control esta secuencia se repite indefinidamente
  }
  else if(finaldecarrera1==HIGH && rele1==HIGH && finaldecarrera2==LOW) //para el porton arriba
  {
    digitalWrite(RELE1,LOW);
    //digitalWrite(13,LOW);
    delay(100);
   
  mySwitch.resetAvailable();// hace que aca se termine la instruccion del control
  }
      if(finaldecarrera2==LOW && rele1==LOW &&finaldecarrera1==HIGH && value==boton2)  //bajo el porton
      {
          digitalWrite(RELE2,HIGH);
      //    digitalWrite(13,HIGH);
          delay(100);
          Serial.println("bajando ");
      }
   else if(finaldecarrera1==LOW && rele2==HIGH && finaldecarrera2==HIGH)  //para el porton abajo
   {
          digitalWrite(RELE2,LOW);
        //  digitalWrite(13,LOW);
          delay(100); 
          mySwitch.resetAvailable();
   }
   if(rele2==HIGH && value==boton1 && finaldecarrera1==HIGH)
          {
           digitalWrite(RELE2,LOW);
           delay(200);
           digitalWrite(RELE1,HIGH); 
           delay(500);
           Serial.print("entre al if de emergencia para subir");
          }
    if(rele1==HIGH && value==boton2 && finaldecarrera2==HIGH && finaldecarrera1==HIGH) // si el porton esta subiendo y toco el boton 2...
    {                                                                             //detengo el porton
        digitalWrite(RELE1,LOW);
        //digitalWrite(13,LOW);
        delay(400);
        Serial.println("detengo el porton en la subida");  
        mySwitch.resetAvailable();
    }
    if(rele1==LOW && rele2==LOW && value==boton1 && finaldecarrera2==HIGH)    //si el porton se detuvo y se toca el boton 1 el porton sube
    {
         digitalWrite(RELE1,HIGH);
        // digitalWrite(13,HIGH);
         delay(200);
         Serial.println("me detuve arriba luego de detenerme en el medio");
    }
     //**********************
     //fin del primer mando**
     //**********************

    if(finaldecarrera1==LOW && value==boton3 && rele2==LOW && finaldecarrera2==HIGH) //sube el porton
  {
   digitalWrite(RELE1,HIGH);
   //digitalWrite(13,HIGH);
   delay(100);
   Serial.println("subiendo ");
   //mySwitch.resetAvailable();   //al no terminarse la instruccion del control esta secuencia se repite indefinidamente
  }
  else if(finaldecarrera1==HIGH && rele1==HIGH && finaldecarrera2==LOW) //para el porton arriba
  {
    digitalWrite(RELE1,LOW);
    //digitalWrite(13,LOW);
    delay(100);
   
  mySwitch.resetAvailable();// hace que aca se termine la instruccion del control
  }
      if(finaldecarrera2==LOW && rele1==LOW &&finaldecarrera1==HIGH && value==boton4)  //bajo el porton
      {
          digitalWrite(RELE2,HIGH);
      //    digitalWrite(13,HIGH);
          delay(100);
          Serial.println("bajando ");
      }
   else if(finaldecarrera1==LOW && rele2==HIGH && finaldecarrera2==HIGH)  //para el porton abajo
   {
          digitalWrite(RELE2,LOW);
        //  digitalWrite(13,LOW);
          delay(100); 
          mySwitch.resetAvailable();
   }
   if(rele2==HIGH && value==boton3 && finaldecarrera1==HIGH)
          {
           digitalWrite(RELE2,LOW);
           delay(200);
           digitalWrite(RELE1,HIGH); 
           delay(500);
           Serial.print("entre al if de emergencia para subir");
          }
    if(rele1==HIGH && value==boton4 && finaldecarrera2==HIGH && finaldecarrera1==HIGH) // si el porton esta subiendo y toco el boton 2...
    {                                                                             //detengo el porton
        digitalWrite(RELE1,LOW);
        //digitalWrite(13,LOW);
        delay(400);
        Serial.println("detengo el porton en la subida");  
        mySwitch.resetAvailable();
    }
    if(rele1==LOW && rele2==LOW && value==boton3 && finaldecarrera2==HIGH)    //si el porton se detuvo y se toca el boton 1 el porton sube
    {
         digitalWrite(RELE1,HIGH);
         //digitalWrite(13,HIGH);
         delay(200);
         Serial.println("me detuve arriba luego de detenerme en el medio");
    }
    
    
          //*********************
          //fin del segundo mando
          //*********************

if(finaldecarrera1==LOW && value==boton7 && rele2==LOW && finaldecarrera2==HIGH) //sube el porton
  {
   digitalWrite(RELE1,HIGH);
   //digitalWrite(13,HIGH);
   delay(100);
   Serial.println("subiendo ");
   //mySwitch.resetAvailable();   //al no terminarse la instruccion del control esta secuencia se repite indefinidamente
  }
  else if(finaldecarrera1==HIGH && rele1==HIGH && finaldecarrera2==LOW) //para el porton arriba
  {
    digitalWrite(RELE1,LOW);
    //digitalWrite(13,LOW);
    delay(100);
   
  mySwitch.resetAvailable();// hace que aca se termine la instruccion del control
  }
      if(finaldecarrera2==LOW && rele1==LOW &&finaldecarrera1==HIGH && value==boton8)  //bajo el porton
      {
          digitalWrite(RELE2,HIGH);
      //    digitalWrite(13,HIGH);
          delay(100);
          Serial.println("bajando");
      }
   else if(finaldecarrera1==LOW && rele2==HIGH && finaldecarrera2==HIGH)  //para el porton abajo
   {
          digitalWrite(RELE2,LOW);
        //  digitalWrite(13,LOW);
          delay(100); 
          mySwitch.resetAvailable();
   }
   if(rele2==HIGH && value==boton7 && finaldecarrera1==HIGH)
          {
           digitalWrite(RELE2,LOW);
           delay(200);
           digitalWrite(RELE1,HIGH); 
           delay(500);
           Serial.print("entre al if de emergencia para subir");
          }
    if(rele1==HIGH && value==boton8 && finaldecarrera2==HIGH && finaldecarrera1==HIGH) // si el porton esta subiendo y toco el boton 2...
    {                                                                             //detengo el porton
        digitalWrite(RELE1,LOW);
        //digitalWrite(13,LOW);
        delay(400);
        Serial.println("detengo el porton en la subida");  
        mySwitch.resetAvailable();
    }
    if(rele1==LOW && rele2==LOW && value==boton7 && finaldecarrera2==HIGH)    //si el porton se detuvo y se toca el boton 1 el porton sube
    {
         digitalWrite(RELE1,HIGH);
         //digitalWrite(13,HIGH);
         delay(200);
         Serial.println("me detuve arriba luego de detenerme en el medio");
    }
          //*************************
          //fin del mando 3
          //*************************


    if(finaldecarrera1==LOW && value==boton5 && rele2==LOW && finaldecarrera2==HIGH) //sube el porton
  {
   digitalWrite(RELE1,HIGH);
   //digitalWrite(13,HIGH);
   delay(100);
   Serial.println("subiendo");
   //mySwitch.resetAvailable();   //al no terminarse la instruccion del control esta secuencia se repite indefinidamente
  }
  else if(finaldecarrera1==HIGH && rele1==HIGH && finaldecarrera2==LOW) //para el porton arriba
  {
    digitalWrite(RELE1,LOW);
    //digitalWrite(13,LOW);
    delay(100);
   
  mySwitch.resetAvailable();// hace que aca se termine la instruccion del control
  }
      if(finaldecarrera2==LOW && rele1==LOW &&finaldecarrera1==HIGH && value==boton6)  //bajo el porton
      {
          digitalWrite(RELE2,HIGH);
      //    digitalWrite(13,HIGH);
          delay(100);
          Serial.println("bajando");
      }
   else if(finaldecarrera1==LOW && rele2==HIGH && finaldecarrera2==HIGH)  //para el porton abajo
   {
          digitalWrite(RELE2,LOW);
        //  digitalWrite(13,LOW);
          delay(100); 
          mySwitch.resetAvailable();
   }
   if(rele2==HIGH && value==boton5 && finaldecarrera1==HIGH)
          {
           digitalWrite(RELE2,LOW);
           delay(200);
           digitalWrite(RELE1,HIGH); 
           delay(500);
           Serial.print("entre al if de emergencia para subir");
          }
    if(rele1==HIGH && value==boton6 && finaldecarrera2==HIGH && finaldecarrera1==HIGH) // si el porton esta subiendo y toco el boton 2...
    {                                                                             //detengo el porton
        digitalWrite(RELE1,LOW);
        //digitalWrite(13,LOW);
        delay(400);
        Serial.println("detengo el porton en la subida");  
        mySwitch.resetAvailable();
    }
    if(rele1==LOW && rele2==LOW && value==boton5 && finaldecarrera2==HIGH)    //si el porton se detuvo y se toca el boton 1 el porton sube
    {
         digitalWrite(RELE1,HIGH);
         //digitalWrite(13,HIGH);
         delay(200);
         Serial.println("me detuve arriba luego de detenerme en el medio");
    }

        //***************
        //fin del mando 4
        //***************
        
      } //cierro un else
      
  } //cierro el if
  
} //cierro la funcion
